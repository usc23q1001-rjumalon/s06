from abc import ABC, abstractclassmethod

class Person(ABC):
	
	@abstractclassmethod

	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_firstName(self):
		print(f'{self._firstName}')

	def get_lastName(self):
		print(f'{self._lastName}')

	def get_email(self):
		print(f'{self._email}')

	def get_department(self):
		print(f'{self._department}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def checkRequest(self):
		pass

	def addRequest(self):
		return "Request has been added"

	def login(self):
		return f"{self._email} has logged in"
		
	def logout(self):
		return f"{self._email} has logged out"
		
	def addUser(self):
		pass


class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._members = []

	def get_firstName(self):
		print(f'{self._firstName}')

	def get_lastName(self):
		print(f'{self._lastName}')

	def get_email(self):
		print(f'{self._email}')

	def get_department(self):
		print(f'{self._department}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def checkRequest(self):
		pass

	def addRequest(self):
		return "Request has been added"

	def login(self):
		return f"{self._email} has logged in"
		
	def logout(self):
		return f"{self._email} has logged out"
		
	def addUser(self):
		pass

	def addMember(self, employee):
		self._members.append(employee)

	def get_members(self):
		return self._members



class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_firstName(self):
		print(f'{self._firstName}')

	def get_lastName(self):
		print(f'{self._lastName}')

	def get_email(self):
		print(f'{self._email}')

	def get_department(self):
		print(f'{self._department}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"
		
	def checkRequest(self):
		pass

	def addRequest(self):
		return "Request has been added"

	def login(self):
		return f"{self._email} has logged in"
		
	def logout(self):
		return f"{self._email} has logged out"
		
	def addUser(self):
		return "User has been added"
		

class Request():
	def __init__(self, name, requester, dateRequest):
		self._name = name
		self._requester = requester
		self._dateRequest = dateRequest
		self._status = "open"

	def get_name(self):
		print(f'{self._name}')

	def get_requester(self):
		print(f'{self._requester}')

	def get_dateRequest(self):
		print(f'{self._dateRequest}')

	def get_status(self):
		print(f'{self._status}')

	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester

	def set_email(self, dateRequest):
		self._dateRequest = dateRequest

	def set_status(self, status):
		self._status = status

	def updateRequest(self):
		return "Updated request"

	def closeRequest(self):
		return f"Request {self._name} has been {self._status}"

	def cancelRequest(self):
		return f"Request {self._name} has been canceled"

employeel = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon'@mail.com", "Sales")
adminl = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLeadl = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
reql = Request("New hire orientation", teamLeadl, "27-Jul-2021")
req2 = Request("Laptop repair", employeel, "1-Jul-2021")

assert employeel.getFullName() == "John Doe"
assert adminl.getFullName() == "Monika Justin"
assert teamLeadl.getFullName() == "Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLeadl.addMember(employee3)
teamLeadl.addMember(employee4)

for indiv_emp in teamLeadl.get_members():
	print(indiv_emp.getFullName())

assert adminl.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())